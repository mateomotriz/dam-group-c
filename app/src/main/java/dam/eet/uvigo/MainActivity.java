package dam.eet.uvigo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wafflecopter.multicontactpicker.ContactResult;
import com.wafflecopter.multicontactpicker.MultiContactPicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactDialog.ContactDialogListener {

    // FIREBASE
    private DatabaseReference database;
    private FirebaseUser user;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int REQUEST_CONTACTS = 3;

    // NOTIFS
    NotificationReminder nr;

    // BDAY LIST
    RecyclerView bdayList;
    PersonasAdapter adapter;
    ArrayList<Persona> pList = new ArrayList<>();

    // NEXT BDAY
    CircularImageView nextBdayAvatar;
    TextView nextBdayName;
    TextView nextBdayDate;

    // CARD LISTENER
    PersonasAdapter.CardsListener cardsListener;

    // FABs
    private FloatingActionButton floatButton, floatNewContact, floatImportContact, floatSignOut;
    private boolean isFabOpen;
    String name, phoneNumber, birthday, email, key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("mateo", "main activity on create");

        setContentView(R.layout.activity_main);
        Toolbar tb = findViewById(R.id.mainToolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tb.setNavigationIcon(R.drawable.ic_exit_to_app);
        tb.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click", "navigation button");
                signOut();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // next bday
        nextBdayAvatar = findViewById(R.id.nextBirthdayAvatar);
        nextBdayName = findViewById(R.id.nextBirthdayName);
        nextBdayDate = findViewById(R.id.nextBirthdayDate);

        // prepare lists
        bdayList = findViewById(R.id.list);
        bdayList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        // get current user for accessing database and database instance that points to the 'users' node
        user = getUser();
        database = FirebaseDatabase.getInstance().getReference("users").child(user.getUid());
        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("click", "on data change");
                pList.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    name = ds.child("name").getValue(String.class);
                    phoneNumber = ds.child("phone").getValue(String.class);
                    birthday = ds.child("birthday").getValue(String.class);
                    email = ds.child("mail").getValue(String.class);
                    key = ds.child("key").getValue(String.class);
                    Persona contact = new Persona(name, birthday, email, phoneNumber, key);
                    pList.add(contact);
                }

                if (!pList.isEmpty()) {
                    Collections.sort(pList);
                    adapter = new PersonasAdapter(pList, MainActivity.this, new PersonasAdapter.CardsListener() {
                        @Override
                        public void expandButtonOnClick(Button b, MaterialCardView mcv, ConstraintLayout cl) {
                            expandButtonListener(b, mcv, cl);
                        }

                        @Override
                        public void deleteButtonOnClick(int position) {
                            deleteButtonListener(position);
                        }
                    });
                    bdayList.setAdapter(adapter);

                    // next bday!
                    nextBdayName.setText(pList.get(0).getName());
                    nextBdayDate.setText(pList.get(0).getBirthday());
                    //nextBdayAvatar.setImageResource();
                }

                // NOTIFs
                nr = new NotificationReminder(MainActivity.this);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    nr.sendNotification(pList.get(0));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("onCancelled()", "database error");
            }
        });

        // get floating button and open extended fab buttons when clicked
        isFabOpen = false;
        floatButton = findViewById(R.id.fab);
        floatNewContact = findViewById(R.id.fabNewUser);
        floatImportContact = findViewById(R.id.fabImportContacts);
        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openExtendedFabs();
            }
        });

        floatNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        floatImportContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showContact();
            }
        });
    }

    // CARD LISTENERs
    private void expandButtonListener(Button b, MaterialCardView mcv, ConstraintLayout cl) {
        if (cl.getVisibility() == View.GONE) {
            androidx.transition.TransitionManager.beginDelayedTransition(mcv, new AutoTransition());
            cl.setVisibility(View.VISIBLE);
            b.setBackgroundResource(R.drawable.ic_arrow_up);
        } else {
            androidx.transition.TransitionManager.beginDelayedTransition(mcv, new AutoTransition());
            cl.setVisibility(View.GONE);
            b.setBackgroundResource(R.drawable.ic_arrow_down);
        }
    }

    private void deleteButtonListener(int i) {
        Persona deleteContact = pList.get(i);
        database.child(deleteContact.getKey()).removeValue();
        Log.d("click", "deleted user: " + deleteContact.getKey());

    }

    // FABs and DIALOG
    private void openExtendedFabs() {
        if (!isFabOpen) {
            showFabMenu();
        } else {
            hideFabMenu();
        }
    }

    private void openDialog() {
        openDialog(false, false, false);
    }

    private void openDialog(boolean importContacts, boolean setMail, boolean setPhone) {
        ContactDialog dialog = new ContactDialog();
        Bundle args = new Bundle();
        if (importContacts) {
            args.putBoolean("importing", true);
            args.putString("name", name);
            if (setMail) {
                args.putString("email", email);
            }
            if (setPhone) {
                args.putString("phone", phoneNumber);
            }
        } else {
            args.putBoolean("importing", false);
        }
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "contact dialog");
    }

    private void showFabMenu() {
        isFabOpen = true;
        floatNewContact.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        floatImportContact.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
    }

    private void hideFabMenu() {
        isFabOpen = false;
        floatNewContact.animate().translationY(0);
        floatImportContact.animate().translationY(0);
    }

    // FIREBASE
    private FirebaseUser getUser() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            return user;
        } else {
            Log.d("getUser()", "Cannot retrieve user's data");
            return null;
        }
    }

    @Override
    public void saveToDatabase(String name, String birthday, String email, String phoneNumber) {
        String key = database.push().getKey();
        Persona contact = new Persona(name, birthday, email, phoneNumber, key);
        database.child(key).setValue(contact);
    }

    private void showContact() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, REQUEST_CONTACTS);
        } else {
            new MultiContactPicker.Builder(MainActivity.this).showPickerForResult(2);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean setPhone = false;
        boolean setMail = false;
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                List<ContactResult> results = MultiContactPicker.obtainResult(data);
                for (ContactResult result : results) {
                    name = result.getDisplayName();
                    if (result.getEmails().size() > 0) {
                        email = result.getEmails().get(0);
                        setMail = true;
                    }
                    if (result.getPhoneNumbers().size() > 0) {
                        phoneNumber = result.getPhoneNumbers().get(0).getNumber();
                        setPhone = true;
                    }
                    openDialog(true, setMail, setPhone);
                    setMail = false;
                    setPhone = false;
                }
            } else if (resultCode == RESULT_CANCELED) {
                System.out.println("User closed the picker without selecting items.");
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new MultiContactPicker.Builder(MainActivity.this).showPickerForResult(2);
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    private void signOut() {
        // Firebase sign out
        mAuth.getInstance().signOut();

        // Google sign out
        GoogleSignIn.getClient(this,
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build())
                .signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(i);
                    }
                });
    }

}