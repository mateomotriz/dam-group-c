package dam.eet.uvigo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputLayout;

public class ContactDialog extends AppCompatDialogFragment {

    private TextInputLayout editName, editPhoneNumber, editBirthday, editEmail;
    private ContactDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle state = getArguments();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_contact_layout, null);

        editName = view.findViewById(R.id.edit_name_field);
        editPhoneNumber = view.findViewById(R.id.edit_number_field);
        editBirthday = view.findViewById(R.id.edit_birthday_field);
        editEmail = view.findViewById(R.id.edit_email_field);

        if (state.getBoolean("importing")) {
            editName.getEditText().setText(state.getString("name"));
            if (state.containsKey("email")) {
                editEmail.getEditText().setText(state.getString("email"));
            }
            if (state.containsKey("phone")) {
                editPhoneNumber.getEditText().setText(state.getString("phone"));
            }
        }

        editBirthday.getEditText().addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (! editBirthday.getEditText().getText().toString().matches(getString(R.string.date_format))) {
                    editBirthday.getEditText().setError(getString(R.string.birthday_error));
                } else {
                    editBirthday.getEditText().setError(null);
                }
            }
        });

        builder.setView(view)
                .setTitle("Add contact")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = editName.getEditText().getText().toString();
                        String phone = editPhoneNumber.getEditText().getText().toString();
                        String birthday = editBirthday.getEditText().getText().toString();
                        if (!birthday.matches(getString(R.string.date_format))){
                            return;
                        }
                        String email = editEmail.getEditText().getText().toString();
                        listener.saveToDatabase(name, birthday, email, phone);
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ContactDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement ContactDialogListener");
        }
    }

    public interface ContactDialogListener {
        void saveToDatabase(String name, String birthday, String email, String phoneNumber);
    }
}
