package dam.eet.uvigo;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.Serializable;

public class NotificationReceiver extends BroadcastReceiver {
    final private String SUBJECT = "Surprise!";
    String customText = "Happy bday my dear friend ";

    @Override
    public void onReceive(Context context, Intent intent) {
        // get data
        Persona p = (Persona) intent.getSerializableExtra("persona");
        customText += p.getName();

        Intent i = new Intent();
        // email & sms
        i.setType("text/plain");
        i.setAction(intent.getAction());
        switch (intent.getStringExtra("type")) {
            case "mail":
                Log.d("notif", "mail");
                Uri mailUri = Uri.parse("mailto:" + p.getMail())
                        .buildUpon()
                        .appendQueryParameter("subject", SUBJECT)
                        .appendQueryParameter("body", customText)
                        .build();
                i.setData(mailUri);
                break;
            case "sms":
                Log.d("notif", "sms");
                i.setData(Uri.parse("smsto:" + p.getPhone()));
                i.putExtra("sms_body", customText);
                i.putExtra(Intent.EXTRA_TEXT, customText);
                break;
            case "share":
                Log.d("notif", "share");
                i.putExtra(Intent.EXTRA_TEXT, customText);
                break;
        }

        Intent shareIntent = Intent.createChooser(i, null);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(shareIntent);

        // close pannel
        Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.sendBroadcast(it);

    }
}
