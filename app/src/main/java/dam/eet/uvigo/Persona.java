package dam.eet.uvigo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Persona implements Serializable, Comparable<Persona> {
    // serializable to put on intents as xtra
    private int avatar;
    private String name, birthday, email, phone, key;

    public Persona(int avatar, String name, String birthday, String email, String phone) {
        this.name = name;
        this.birthday = birthday;
        this.email = email;
        this.phone = phone;
    }

    public Persona(String name, String birthday, String email, String phone) {
        this(-1, name, birthday, email, phone);
    }

    public Persona(String name, String birthday, String email, String phone, String key) {
        this(name, birthday, email, phone);
        this.key = key;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMail() {
        return this.email;
    }

    public void setMail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date BirthdayDate() {
        String[] parts = birthday.split("/");
        int day = Integer.parseInt(parts[0]);
        int month = Integer.parseInt(parts[1]);
        return new Date(Calendar.getInstance().get(Calendar.YEAR)-1900, month-1, day);

    }

    @Override
    public int compareTo(Persona p) {
        final long now = System.currentTimeMillis();
        Date currentDate = new Date(now);
        Date thisBirthday = this.BirthdayDate();
        Date pBirthday = p.BirthdayDate();

        if (this.BirthdayDate().getMonth() < currentDate.getMonth()
            || (this.BirthdayDate().getMonth() == currentDate.getMonth()
                && this.BirthdayDate().getDate() < currentDate.getDate())){
            thisBirthday.setYear(this.BirthdayDate().getYear() + 1);
        }
        if (pBirthday.getMonth() < currentDate.getMonth()
                || (pBirthday.getMonth() == currentDate.getMonth()
                && pBirthday.getDate() < currentDate.getDate())){
            pBirthday.setYear(this.BirthdayDate().getYear() + 1);
        }

        long diff1 = Math.abs(thisBirthday.getTime() - now);
        long diff2 = Math.abs(pBirthday.getTime() - now);
        if (diff1 < diff2)
            return -1;
        else if (diff1 == diff2)
            return 0;
        else
            return 1;
    }
}
