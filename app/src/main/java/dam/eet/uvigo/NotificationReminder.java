package dam.eet.uvigo;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static androidx.core.content.res.TypedArrayUtils.getString;

public class NotificationReminder {
    Activity activity;

    // NOTIFICATIONS
    private final String CHANNEL_ID = "bday";
    private final int NOTIF_ID = 14;
    private final NotificationManagerCompat nmc;

    public NotificationReminder(Activity activity) {
        this.activity = activity;
        createNotificationChannel(activity);
        nmc = NotificationManagerCompat.from(activity.getApplicationContext());
    }

    // NOTIFS
    public void createNotificationChannel(Activity activity) {
        String channel_name = "materoids";
        String channel_description = "4 >= Android Oreo";

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, channel_name, importance);
            channel.setDescription(channel_description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            Activity a = new Activity();
            NotificationManager notificationManager = activity.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            // customize
            // channel.enableLights(true);
            // channel.setLightColor(R.color.Aquamarine);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void sendNotification(Persona persona) {
        // bundle w persona
        Bundle b = new Bundle();

        // email
        Intent brxIntent1 = new Intent(activity.getApplicationContext(), NotificationReceiver.class);
        brxIntent1.setAction(Intent.ACTION_SENDTO);
        brxIntent1.putExtra("persona", persona);
        brxIntent1.putExtra("type", "mail");
        PendingIntent mailPendingIntent =
                PendingIntent.getBroadcast(activity.getApplicationContext(), 1, brxIntent1, PendingIntent.FLAG_CANCEL_CURRENT);

        // sms
        Intent brxIntent2 = new Intent(activity.getApplicationContext(), NotificationReceiver.class);
        brxIntent2.setAction(Intent.ACTION_SENDTO);
        brxIntent2.putExtra("persona", persona);
        brxIntent2.putExtra("type", "sms");
        PendingIntent smsPendingIntent =
                PendingIntent.getBroadcast(activity.getApplicationContext(), 2, brxIntent2, PendingIntent.FLAG_CANCEL_CURRENT);

        // share
        Intent brxIntent3 = new Intent(activity.getApplicationContext(), NotificationReceiver.class);
        brxIntent3.setAction(Intent.ACTION_SEND);
        brxIntent3.putExtra("persona", persona);
        brxIntent3.putExtra("type", "share");
        PendingIntent sharePendingIntent =
                PendingIntent.getBroadcast(activity.getApplicationContext(), 3, brxIntent3, PendingIntent.FLAG_CANCEL_CURRENT);


        // if w hit the notif and then 'back', we'll continue where we were before
        Intent resultIntent = new Intent(activity, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(activity);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, FLAG_UPDATE_CURRENT);

        // send notif
        String title = String.valueOf((R.string.app_name));
        Notification n = new NotificationCompat.Builder(activity, CHANNEL_ID)
                .setSmallIcon(R.drawable.icon_notifs)
                .setLargeIcon(BitmapFactory.decodeResource(activity.getApplicationContext().getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("BdayReminder")
                .setContentText("Next Bday: " + persona.getName() + " [" + persona.getBirthday() + "]")
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .setColor(Color.argb(0, 50, 50, 50))
                .addAction(R.drawable.ic_mail, activity.getApplicationContext().getString(R.string.notif_mail), mailPendingIntent)
                .addAction(R.drawable.ic_sms, activity.getApplicationContext().getString(R.string.notif_sms), smsPendingIntent)
                .addAction(R.drawable.ic_share, activity.getApplicationContext().getString(R.string.notif_share), sharePendingIntent)
                .build();
        nmc.notify(NOTIF_ID, n);
    }
}
