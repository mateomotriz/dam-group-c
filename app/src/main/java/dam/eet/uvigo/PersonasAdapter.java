package dam.eet.uvigo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PersonasAdapter extends RecyclerView.Adapter<PersonasAdapter.ViewHolder> {
    // PARAMETERS
    Activity activity;
    ArrayList<Persona> list;

    // COLORS
    private final int c1 = Color.rgb(188, 31, 255); //purple
    private final int c2 = Color.rgb(50, 168, 82); //green
    private final int c3 = Color.rgb(255, 31, 72); //red
    private final int c4 = Color.rgb(31, 154, 255); //blue
    private final int c5 = Color.rgb(255, 242, 0); //yellow
    int[] colorsList = {c1, c2, c3, c4, c5};
    int colorSelector = 0;


    public PersonasAdapter(ArrayList<Persona> list, Activity activity, CardsListener listener) {
        this.list = list;
        this.activity = activity;
        onClickListener = listener;
    }

    // LISTENER
    public CardsListener onClickListener;

    public interface CardsListener {

        void expandButtonOnClick(Button b, MaterialCardView mcv, ConstraintLayout cl);

        void deleteButtonOnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView subtitle;
        TextView email_layout;
        TextView phone_layout;
        CircularImageView image;

        // EXPANDABLE CARD
        ConstraintLayout expandableView;
        MaterialCardView cardView;
        Button arrowButton;
        Button deleteButton;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.bday_title);
            subtitle = itemView.findViewById(R.id.bday_subtitle);
            email_layout = itemView.findViewById(R.id.mailUser);
            phone_layout = itemView.findViewById(R.id.phoneUser);
            image = itemView.findViewById(R.id.avatar);

            // EXPANDABLE CARD
            expandableView = itemView.findViewById(R.id.expandableView);
            cardView = itemView.findViewById(R.id.row);

            // BUTTON LISTENERS
            arrowButton = itemView.findViewById(R.id.expandButton);
            deleteButton = itemView.findViewById(R.id.deleteButton);

            arrowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.expandButtonOnClick(arrowButton, cardView, expandableView);
                }
            });
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.deleteButtonOnClick(getAdapterPosition());
                }
            });
        }

        public void getData(Persona persona) {
            int avatar = persona.getAvatar();
            String name = persona.getName();
            String email = persona.getMail();
            String phone = persona.getPhone();
            String bday = persona.getBirthday();
            Random rnd = new Random();

            // avatar
            int avatarColor = colorsList[colorSelector];
            colorSelector++;
            if (colorSelector == colorsList.length) {
                colorSelector = 0;
            }
            image.setBorderColor(avatarColor);
            title.setTextColor(avatarColor);
            //image.setImageResource(avatar);

            // name & bday
            title.setText(name);
            subtitle.setText(bday);
            email_layout.setText(email);
            phone_layout.setText(phone);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        // inflate row
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.bday_elements, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        holder.getData(list.get(i));
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }

    @Override
    public int getItemCount() {
        try {
            return list.size();
        } catch (NullPointerException e) {
            Log.e("PersonasAdapter", "empty list");
            return 0;
        }
    }
}
